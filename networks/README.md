# Networks

## Overview

This directory provides network implementations for the topologies described in the paper. The
topologies are invoked via the top-level minimega startup script. For example:

```
cd minimega
sudo ./start.sh ../networks/dqm/topo.sh
```

You need to wait for the network to initialize before you can interact with it or start traffic. The
easiest way to see that the network has initialized is to open a web browser to miniweb on
`localhost:9001` (if you are running on a non-local machine, set up an SSH tunnel on port 9001), and
wait for the control network IP addresses (`8.0.0.0/16`) to populate:
![](../png/miniweb.png)

Once
they have done so, run the following script to add the VM control DNS names to your machine's
`/etc/hosts`:
```
sudo ../misc/generate-etchosts.sh
cat hosts.txt | sudo tee -a /etc/hosts
```

You should also ensure that your `~/.ssh/config` has this setting to allow you to ssh to your nodes
via their control names:
 ```
Match exec "getent hosts %h | grep -qE '^8\.0'"
    User searchlight
    IdentityFile <root of this repository>/ssh/id_rsa
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```

Once this is done, you can reach your VMs by appending `-ctrl` to their hostname;  e.g.,
```
ssh h0-ctrl
```

## Double dummbell

The double-dummbell topology is defined in [double-dumbbell/topo.sh](double-dumbbell/topo.sh)
![](../png/double-dumbbell.png)

## Star

The star topology is defined in [star/topo.sh](star/topo.sh)
![](../png/star.png)

## Loop

The loop topology is defined in [loop/topo.sh](loop/topo.sh)
![](../png/loop.png)

The loop can be configured to route traffic symmetrically or asymmetrically, and the number of
enclaves (arms of the network) can also be configured. This is done via environment variables. For
example, to run an asymmetric routing configuration with 5 nodes, do:

```
cd minimega
sudo -E ASYMMETRIC=1 K=5 ./start.sh ../topos/loop.sh
```

The default settings for the topology are `ASYMMETRIC=0` and `K=4`

## DQM 

The DQM topology is defined in [dqm/topo.sh](dqm/topo.sh)
![](../png/dqm.png)

To create the topology:
```
cd minimega
sudo ./start.sh ../networks/dqm/topo.sh
```

You then need to run an ansible playbook to initialize the virtual OpenFlow switches and the DPDK 
application running in the actuator nodes. Once the control network is up and you've populated your 
`/etc/hosts` and `~/.ssh/config` (as described [above](#overview)), run:
```
ansible-playbook -i ../networks/dqm/hosts ../networks/dqm/setup.yml
```
