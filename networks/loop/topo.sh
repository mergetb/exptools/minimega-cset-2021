#!/bin/bash

ASYMMETRIC=${ASYMMETRIC:-0}
K=${K:-5}

function build_enclave()
{
    local eid="$1"
    local h="h$eid"
    local b="b$eid"
    local a="a$eid"
    local c="c$eid"

    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-client-gui_2021-07-08.qcow2
    new_vm $h

    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2
    new_vm $b
    new_vm $a
    new_vm $c

    connect_vms $h $b
    connect_vms $b $a
    connect_vms $a $c
}

function configure_enclave()
{
    local eid="$1"
    local h="h$eid"
    local b="b$eid"
    local a="a$eid"
    local c="c$eid"

    ###
    # configure end-hosts and border router addressing
    hmac=$(get_mac_addr $h 1)
    hip="10.0.$eid.100"
    bip="10.0.$eid.1"

    set_minirouter_info $b 1 $bip 24
    set_dnsmasq_client $b $bip $h $hmac $hip

    ###
    # enable NAT on border
    enable_nat $b

    ###
    # set the rest of the upstream routes using ospf
    set_minirouter_info $b 2 "11.0.$eid.2" 24
    set_minirouter_info $a 1 "11.0.$eid.1" 24
    set_minirouter_info $a 2 "12.0.$eid.2" 24

    MM_CMD router $b route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $b commit

    MM_CMD router $a route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $a commit
}

function configure_core()
{
    nic_counts=()
    for i in $(seq 0 $((K-1))); do
        nic_counts+=(0)
    done

    for i in $(seq 0 $((K-1))); do
        j=$(( (i + 1) % K))
        ci="c$i"
        cj="c$j"
        ipi="13.$i.$j.1"
        ipj="13.$i.$j.2"
        nici=$((2+${nic_counts[$i]}))
        nicj=$((2+${nic_counts[$j]}))

        set_minirouter_info $ci $nici $ipi 24

        if [ $ASYMMETRIC -eq 1 ]; then
            set_minirouter_info_cost $cj $nicj $ipj 24 1000
        else
            set_minirouter_info $cj $nicj $ipj 24
        fi

        (( nic_counts[$i]++ ))
        (( nic_counts[$j]++ ))
    done

    for i in $(seq 0 $((K-1))); do
        c="c$i"
        set_minirouter_info $c 1 "12.0.$i.1" 24
        MM_CMD router $c route static 0.0.0.0/0 8.0.0.1
        MM_CMD router $c commit
    done
}

###
# Build VMs and specify connectivity
MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2

for i in $(seq 0 $((K-1))); do
    build_enclave $i
done

for i in $(seq 0 $((K-1))); do
    ci="c$i"
    cj="c$(((i+1) % K))"
    connect_vms $ci $cj
done

###
# Launch VMs
for i in $(seq 0 $((K-1))); do
    launch_vm h$i
    launch_vm b$i
    launch_vm a$i
    launch_vm c$i
done

###
# Configure routing
for i in $(seq 0 $((K-1))); do
    configure_enclave $i
done

configure_core
