#!/bin/bash

function build_enclave()
{
    local eid="$1"
    local h0="h$((eid*2))"
    local h1="h$((eid*2+1))"
    local b="b$eid"
    local a="a$eid"
    local r="r$eid"
    local c="c"

    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-client-gui_2021-07-08.qcow2
    new_vm $h0
    new_vm $h1

    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2
    new_vm $b
    new_vm $a
    new_vm $r

    connect_vms $b $h0
    connect_vms $b $h1
    connect_vms $b $a
    connect_vms $a $r
    connect_vms $r $c
}

function configure_enclave()
{
    local eid="$1"
    local b="b$eid"
    local a="a$eid"
    local r="r$eid"

    ###
    # configure end-hosts and border router addressing
    for i in $(seq 0 1); do
        h="h$((eid*2+$i))"
        hmac=$(get_mac_addr $h 1)
        hip="10.$i.$eid.100"
        bip="10.$i.$eid.1"

        set_minirouter_info $b $((1+$i)) $bip 24
        set_dnsmasq_client $b $bip $h $hmac $hip
    done

    ###
    # enable NAT on border
    enable_nat $b

    ###
    # set the rest of the upstream routes using ospf
    set_minirouter_info $b 3 "11.0.$eid.2" 24
    set_minirouter_info $a 1 "11.0.$eid.1" 24
    set_minirouter_info $a 2 "12.0.$eid.2" 24
    set_minirouter_info $r 1 "12.0.$eid.1" 24
    set_minirouter_info $r 2 "13.0.$eid.2" 24

    MM_CMD router $b route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $b commit

    MM_CMD router $a route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $a commit

    MM_CMD router $r route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $r commit
}

function configure_core()
{
    set_minirouter_info c 1 "13.0.0.1" 24
    set_minirouter_info c 2 "13.0.1.1" 24
    set_minirouter_info c 3 "13.0.2.1" 24
    set_minirouter_info c 4 "13.0.3.1" 24
    MM_CMD router c route static 0.0.0.0/0 8.0.0.1
    MM_CMD router c commit
}

###
# Build VMs and specify connectivity
MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2
new_vm c

for i in $(seq 0 3); do
    build_enclave $i
done

###
# Launch VMs
for i in $(seq 0 7); do
    launch_vm h$i
done

for i in $(seq 0 3); do
    launch_vm b$i
    launch_vm a$i
    launch_vm r$i
done
launch_vm c

###
# Configure routing
for i in $(seq 0 3); do
    configure_enclave $i
done
configure_core
