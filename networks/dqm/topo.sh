#!/bin/bash

V2V=${V2V:-1}

function build_enclave()
{
    local eid="$1"
    local h0="h$((eid*2))"
    local h1="h$((eid*2+1))"
    local b="b$eid"
    local s="s$eid"
    local d="d$eid"
    local c="c0"

    MM_CMD vm config vcpus 1
    MM_CMD vm config memory 1024
    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-client-gui_2021-07-08.qcow2
    new_vm $h0
    new_vm $h1

    MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2
    new_vm $b
    new_vm_ctrl_dns $s

    MM_CMD vm config vcpus 4
    MM_CMD vm config memory 2048
    new_vm_ctrl_dns $d

    connect_vms $b $h0
    connect_vms $b $h1

    connect_vms $s $b
    connect_vms $s $c
    connect_vms $s $d
    connect_vms $s $d # 2 s/d links
}

function configure_enclave()
{
    local eid="$1"
    local b="b$eid"
    local s="s$eid"
    local d="d$eid"

    ###
    # configure end-hosts and border router addressing
    for i in $(seq 0 1); do
        h="h$((eid*2+$i))"
        hmac=$(get_mac_addr $h 1)
        hip="10.$i.$eid.100"
        bip="10.$i.$eid.1"

        set_minirouter_info $b $((1+$i)) $bip 24
        set_dnsmasq_client $b $bip $h $hmac $hip
    done

    ###
    # enable NAT on border
    enable_nat $b

    ###
    # set the upstream route using ospf
    set_minirouter_info $b 3 "11.0.$eid.2" 24
    MM_CMD router $b route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $b commit

    MM_CMD router $s route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $s commit

    MM_CMD router $d route static 0.0.0.0/0 8.0.0.1
    MM_CMD router $d commit

    if [ $V2V -eq 1 ]; then
        ###
        # Make s/d links v2v
        mark_v2v $s 3 $d 1
        mark_v2v $s 4 $d 2
    fi
}

function configure_core()
{
    set_minirouter_info c0 1 "11.0.0.1" 24
    set_minirouter_info c0 2 "11.0.1.1" 24
    MM_CMD router c0 route static 0.0.0.0/0 8.0.0.1
    MM_CMD router c0 commit
}

###
# Build VMs and specify connectivity
MM_CMD vm config disk $IMG/ubuntu18.04-searchlight-router.qcow2
new_vm c0

for i in $(seq 0 1); do
    build_enclave $i
done

###
# Launch VMs
for i in $(seq 0 3); do
    launch_vm h$i
done

for i in $(seq 0 1); do
    launch_vm b$i
    launch_vm s$i
    launch_vm d$i
done
launch_vm c0

###
# Configure routing
for i in $(seq 0 1); do
    configure_enclave $i
done
configure_core
