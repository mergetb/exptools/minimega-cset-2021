# Networks

## Overview

This directory provides traffic models for a subset of the experiments described in the paper. Before
deploying these traffic models, a [network](../networks) must first be deployed.

Traffic is deployed through the [start-traffic.py](start-traffic.py) python script:
```
briankoco@leviathan traffic]$ sudo ./start-traffic.py -h
Usage: ./start-traffic.py [OPTIONS] <traffis JSON>
  (-h, --help)  : print help and exit
  (-t, --time=) : time (in s) after traffic starts to end of experiment (default: 120)
```

The script accepts a single JSON file as input, which describes the applications to deploy. The
script also accepts the `-t` optional argument to specify how long to let traffic run for before
stopping it. The default is 120 seconds.

### File transfers

The [models/file-transfer](models/file-transfer) directory provides example configurations for 5 variants of the file
transfer application:
- ftp
- ftps
- http
- https
- scp

e.g., the HTTP example:
```
{
  "file-xfer-http": {
    "h0": [
      {
        "target": "h2",
        "params": {
          "client": {
            "s": "500ms",
            "u": "1s",
            "min": "300ms",
            "max": "5s"
          },
          "server": {
            "httproot": "/root/www/"
          }
        }
      }
    ]
  }
}
```

This shows the `file-xfer-http` application being deployed on one client, `h0`, whose transfer is
served by `h2`. Parameters for the client include:
- `s`: standard deviation between transfers
- `u`: mean time between transfers
- `min`/`max`: min/max time between transfers

The server serves from `/root/www/` in the `h2` VM. This value should not be changed.

To run the HTTP transfer:
```
sudo ./start-traffic.py models/file-transfer/http.json
```

### SSH text editing

The [models/ssh-text-edit](models/ssh-text-edit) directory provides example configurations for the
SSH text editing application:
- `slow.json`
- `medium.json`
- `fast.json`

The file names correspond to the speed with which the user types over the SSH session. In each
example, two separate connections are established between 2 sets of clients/servers: one that
emulates a "bursty" typing flow, and one with a "continuous" typing flow. 

e.g., the "fast" example:
```
{
  "ssh-text-edit": {
    "h0": [
      {
        "target": "h2",
        "params": {
          "client": {
            "bursty": "true",
            "speed": "fast"
          },
          "server": {
            "": ""
          }
        }
      }
    ],
    "h1": [
      {
        "target": "h3",
        "params": {
          "client": {
            "bursty": "false",
            "speed": "fast"
          },
          "server": {
            "": ""
          }
        }
      }
    ]
  }
}
```

To run the "fast" example:
```
sudo ./start-traffic.py models/ssh-text-edit/fast.json
```

To see the SSH text edit application in action, point your web browser to `localhost:9001` and
open a VNC session to either VM `h0` or `h1`. You should see sample text being typed over an 
SSH connection in the terminal application, as in the following image:
![](../png/ssh-text-edit.png)

### Video streaming

The [models/video-streaming](models/video-streaming) directory provides example configurations for
the video streaming application:
- `dash.json`
- `hls.json`
- `http.json`

The file names correspond to the video streaming protocol in use: DASH, HLS, and native HTTP
streaming through HTML5. As in the ssh-text-edit examples, each file includes two separate
client/server connections. One connection, between `h0` and `h2` is streamed at 1080p resolution,
while the other, between `h1` and `h3`, is streamed at 576p.

e.g., the Dash example:
```
{
  "video-streaming": {
    "h0": [
      {
        "target": "h2",
        "params": {
          "client": {
            "port": "8001",
            "resolution": "1080",
            "protocol": "dash",
            "time": "3"
          },
          "server": {
            "": ""
          }
        }
      }
    ],
    "h1": [
      {
        "target": "h3",
        "params": {
          "client": {
            "port": "8001",
            "resolution": "576",
            "protocol": "dash",
            "time": "3"
          },
          "server": {
            "": ""
          }
        }
      }
    ]
  }
}
```

To run the DASH example:
```
sudo ./start-traffic.py models/video-streaming/dash.json
```

To see the video streaming application in action, point your web browser to `localhost:9001` and
open a VNC session to either VM `h0` or `h1`. You should see the "Big Buck Bunny" video streaming, 
as in the following image:
![](../png/bbb.png)

### Composing applications

The examples in this repository can be built on to generate combinations of different applications
communicating across any arbitrary set of end-host VMs. 
