#!/bin/bash

export SEARCHLIGHT_HOME=/home/searchlight

# clear out home directory
rm -rf $SEARCHLIGHT_HOME/client-watch-video.tar.gz
rm -rf $SEARCHLIGHT_HOME/client-watch-video

# copy tarball from `cc send` to home directory
#
# hello_searchlight uses `hs_miniccc_files` and september_exercises uses
# `miniccc_files`: the wildcard will handle both.
cp /tmp/miniccc/files/*miniccc_files/video-streaming/client-watch-video.tar.gz $SEARCHLIGHT_HOME

# change into home directory and extract tarball
cd $SEARCHLIGHT_HOME
tar -xf client-watch-video.tar.gz

# sleep for 10s before attempting to run the client-watch-video script
# otherwise we might run into some race conditions where the server
# hasn't started yet
sleep 10
