#!/bin/bash

# clear out web directory
rm -rf /root/www-video

# copy tarball from `cc send` to home directory
#
# hello_searchlight uses `hs_miniccc_files` and september_exercises uses
# `miniccc_files`: the wildcard will handle both.
cp /tmp/miniccc/files/*miniccc_files/video-streaming/www.tar.gz /root/

# change into home directory and extract tarball
cd /root/
mkdir -p /root/www-video

# www/ was the original directory for www.tar.gz, but the next command
# will "rename" it to www-video/
tar -xf www.tar.gz -C www-video --strip-components 1

# create symlink to serve video files
cd /root/www-video/data
ln -s /opt/video_bbb bbb
