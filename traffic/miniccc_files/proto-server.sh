#!/bin/bash

httpdir=/root/www

mkdir -p $httpdir

if [ ! -f $httpdir/bigfile.png ]; then
    dd if=/dev/urandom of=$httpdir/bigfile.png count=1024 bs=1M
fi

echo "<img src=bigfile.png> " > $httpdir/index.html


# generate varying sized blobs for DQM scenario 3b. blobs are not valid
# HTML, so that we can consolidate traffic to one GET request.
#
# see
# https://gitlab.com/searchlight/all/dqm-eval-phase1/-/blob/master/scripts/scenarios/3b/create-sub-scenarios.sh
declare -a sizes=(75 45 15 12 9 6 3)

for i in ${sizes[@]}; do
    mkdir -p ${httpdir}/${i}MB
    dd if=/dev/urandom of=${httpdir}/${i}MB/index.html count=${i} bs=1M
done
