import json
import sys
import random
from os import sep,mkdir,getcwd,chdir,chmod,makedirs
from os.path import exists
import copy
import subprocess

serveFilesDir="miniccc_files"+sep
baseDir="/tmp/minimega/files"+sep

# This python script takes the argument of a json config file
# The config file defines the application used and the clients to server communications. See end of script for config file example

print("See https://gitlab.com/searchlight/mmod-deploy/-/wikis/Protonuke%20Documentation for supported protonuke flags \n")
# returns a random rgb value. i.e #FFFFFF
def writeSCP(server):
    filename= 'exec_scp_'+str(random.randint(0,6292020))+'.sh'
    print("%s" % baseDir+serveFilesDir+filename)
    with open(baseDir+serveFilesDir+filename,'w') as f:
            #exec_scp.sh
        f.write("tar  -xf /tmp/miniccc/files/"+serveFilesDir+"/samples.tar.gz -C /root/\n")
        f.write("for (( ; ; )) do\n")
        f.write("\tls /root/samples/ |sort -R |tail -1 |while read file; do\n")

        f.write("\t\techo \"$file\"\n")
        f.write("\t\tscp -P 22 -o StrictHostKeyChecking=no -i /home/searchlight/.ssh/id_rsa /root/samples/$file searchlight@"+server+"://tmp/\n")
        f.write("\t\tsleep 3\n")
        f.write("\tdone\n")
        f.write("done")
    #chmod(filename,755)
    return(filename)

def runTextEdit(client, server, params):
    # ssh-text-edit doesn't fit into the protonuke-style framework, with
    # commands being run with minimega. for DQM, we'll take care of it
    # inside `run-scenario.py` and replace this function with a noop.
    #
    # the `ssh-text-edit` sections still need to be processed, however,
    # for MockTA and iographs.
    return(None)

def getApp(app):

    if "ftps" in app:
        convApp="ftps"
    elif "https" in app:
        convApp="https"
    elif "ftp" in app:
        convApp="ftp"
    elif "http" in app:
        convApp="http"
    elif "ssh-session" in app:
        convApp="ssh"
    elif "scp" in app:
        convApp="scp"
    elif "web-browse-secure" in app:
        convApp="https"
    elif "web-browse" in app:
        convApp="http"
    elif "email-smtp" in app:
        convApp="smtp"
    elif "irc" in app:
        convApp="irc"
    elif "text-edit" in app:
        convApp="ssh-text-edit"
    elif "video-streaming" in app:
        convApp="video-streaming"
    else:
        print("Application input not recognized: ", app)
        return(app)
    print("Application input %s recognized. Valid App: %s"%(app,convApp))

    return(convApp)

def random_rgb():
    res = "#"
    for i in range(3):
        num = random.randint(0, 255)
        res += "{0:0{1}X}".format(num, 2)
    return res

def getDefaultPnukeParameters(app):
    app=getApp(app)
    #httprootXfer="/root/www/"

    #print("Setting httproot to ",httproot)
    ftpport= "21"
    ftpsport="990"
    sshport="22"
    ftpfilesize="500MB"

    #time between client events. See protonuke documentation
    stddev="-s 500ms"
    mean="-u 1s"
    min="-min 300ms"
    max= "-max 5s"
    #serveFilesDir="hs_miniccc_files/"
    eventTime=" ".join((stddev,mean,min,max))
    switcherCli ={
        "ftp":  " -ftpport "+ftpport +" " +eventTime,
        "ftps": " -ftpsport "+ftpsport +" " +eventTime,
        "http":  eventTime,
        "https": eventTime,
        "ssh":" -sshport "+sshport+" " +eventTime,
        "scp": "",
        "smtp": eventTime,
        "irc": eventTime,
        "ssh-text-edit": "",
        "video-streaming": " "

    }
    switcherServ ={
        "ftp":  " -ftpfilesize "+ftpfilesize+ " -ftpport "+ftpport ,
        "ftps":" -ftpfilesize "+ftpfilesize+ " -ftpsport "+ftpsport +eventTime,
        #"file-xfer-http":" -httproot "+httprootXfer,
        #"file-xfer-https":" -httproot "+httprootXfer,
        "http":"",
        "https":"",
        "ssh":" -sshport "+sshport,
        "scp": "",
        "smtp": "",
        "irc": "",
        "ssh-text-edit": "",
        "video-streaming": " "

    }

    return(switcherCli.get(app,"invalid app"), switcherServ.get(app,"invalid app"))

#get the parameters provided by the config file
def getPnukeParameters(cliParams,srvParams):
    #ToDo, implement validity checking of parameters.
    print("CAUTION!! Currently no validation checking for parameters provided.")
    cliCmds=""
    srvCmds=""
    for key in cliParams:
        cliCmds=cliCmds+ " -"+key + " "+cliParams[key]
    #print("Proto-Client parameters from config: ",cliCmds)
    for key in srvParams:
        srvCmds=srvCmds+ " -"+key + " "+srvParams[key]
    #print("Proto-Serve parameters from config: ",srvCmds)

    return(cliCmds,srvCmds)

def appSwitchServer(app,parameters,userParams):
    #set to an empty string if no httproot is necessary
    #httproot=""
    app=getApp(app)
    switcher ={
        "ftp": "cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+"  "+parameters+" -logfile proto_server_"+app+".log -level debug" ,
        "ftps":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" "+parameters+" -logfile proto_server_"+app+".log -level debug",
        "http": (
            "cc send "+serveFilesDir+"proto-server.sh\n" +
            "cc send "+serveFilesDir+"proto-server-webPages.sh\n" +
            "cc send "+serveFilesDir+"webPages.tar.gz\n" +
            "cc exec bash /tmp/miniccc/files/"+serveFilesDir+"proto-server.sh\n" +
            "cc exec bash /tmp/miniccc/files/"+serveFilesDir+"proto-server-webPages.sh\n" +
            "cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" -logfile proto_server_"+app+".log "+parameters+" -level debug"
            ),
        "https": (
            "cc send "+serveFilesDir+"proto-server.sh\n" +
            "cc send "+serveFilesDir+"proto-server-webPages.sh\n" +
            "cc send "+serveFilesDir+"webPages.tar.gz\n" +
            "cc exec bash /tmp/miniccc/files/"+serveFilesDir+"proto-server.sh\n" +
            "cc exec bash /tmp/miniccc/files/"+serveFilesDir+"proto-server-webPages.sh\n" +
            "cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" -logfile proto_server_"+app+".log "+parameters+" -level debug"
            ),
        "ssh":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" -logfile proto_server_"+app+".log "+parameters+" -level debug",
        "scp":"None",
        "smtp":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" -logfile proto_server_"+app+".log "+parameters+" -level debug",
        "irc":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -serve -"+app+" -logfile proto_server_"+app+".log "+parameters+" -level debug",
        "ssh-text-edit":"None",
        "video-streaming": (
            "cc send "+serveFilesDir+"video-streaming/www.tar.gz\n"+"cc send "+serveFilesDir+"video-streaming/extract-www-tar.sh\n" +
            "cc exec bash -c \"/tmp/miniccc/files/"+serveFilesDir+"video-streaming/extract-www-tar.sh\"\n" +
            "cc background /root/www-video/run.sh"
            )
    }
    return(switcher.get(app,"invalid app on server"))

def appSwitchClient(app,server,parameters,userParams):
    #use server IP and not host Name since there are multiple IPs associated to a host name
    #eventTime=" ".join((stddev,mean,min,max))
    #print(userParams)
    app=getApp(app)
    switcher ={
        "ftp": "cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server ,
        "ftps":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log  -"+app+"  "+server,
        "http":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server,
        "https":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server,
        "ssh":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server,
        "scp":"cc send "+serveFilesDir+"samples.tar.gz\n" + "cc send "+serveFilesDir+userParams["Filename"]+"\n" + "cc background bash /tmp/miniccc/files/"+serveFilesDir+userParams["Filename"],
        "smtp":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server,
        "irc":"cc background /tmp/miniccc/files/"+serveFilesDir+"protonuke -level debug "+parameters+" -logfile proto_client_"+app+".log -"+app+"  "+server,
        "ssh-text-edit":"None",

        "video-streaming": "cc send "+serveFilesDir+"video-streaming/client-watch-video.tar.gz\n"+
        "cc send "+serveFilesDir+"video-streaming/extract-client-watch-video-tar.sh\n"+
        "cc exec bash -c \"/tmp/miniccc/files/"+serveFilesDir+"video-streaming/extract-client-watch-video-tar.sh\"\n"+

        "cc background sudo -u searchlight /home/searchlight/client-watch-video/run.sh --server " +server+":"+userParams["port"] +\
            " --resolution "+userParams["resolution"]+" --protocol " +userParams["protocol"]+ " --time "+userParams["time"]
        }
    return(switcher.get(app,"invalid app on client"))

def validateTraffic(trafficDict,supApps):

    for traffType in trafficDict:
        try:
            supApps["Applications"][traffType]
            print("Success! %s is a supported application type"%traffType)
        except:
            print("%s is not a supported applicaiton type"%traffType)
            print("Please use the follwing apps in your traffic generation config file: %s"%trafficConf)
            print("===========Supported Applications==============")
            for key, value in supApps["Applications"].items() :
                print(key)
            print("================================================")

            sys.exit()

def getVideoEditParams(cliParams,svrParams):
    print(cliParams)
    print(svrParams)


if len(sys.argv)==3:
    trafficConf = sys.argv[1]
    supportedApps=sys.argv[2]

else:
    print("Usage: python Auto-Traffic-Generator.py <proto-traffic-conf.json> <Supported_Applications.json>")
    sys.exit()

with open(trafficConf) as f:
    traffic_dict= json.load(f)

with open(supportedApps) as f:
    sup_apps=json.load(f)

userParams={}
def initUserParams():
    userParams["Filename"]="NA"
    userParams["port"]="NA"
    userParams["resolution"]="NA"
    userParams["time"]="NA"
    userParams["protocol"]="NA"
    userParams["bursty"]="NA"
    userParams["speed"]="NA"
    return(userParams)

userParams= initUserParams()
validateTraffic(traffic_dict,sup_apps)

app_PORTS={}
try:
    for app in sup_apps["Applications"]:
        #print(app)
        keyValues=[]
        for nestedList in sup_apps["Applications"][app]["Definition"]["KeyValueAnd"]:
            #print(nestedList["Value"])
            keyValues.append(nestedList["Value"])
        app_PORTS[app]=keyValues
except:
    print("Applications format does not match")

#%%


commandList=[]
commandDict={}

clients = set({})

#======For Operator Intent DQM========
RulesList=[]
RulesListUp=[]
RulesListDown=[]
uniqueRules=[]
#======For Operator Intent DQM========

#loop through the applications in the json file. FTP,HTTP,HTTPS...
for app in traffic_dict:
    #print((app))

#for each application, loop through clients that will connect with a server
    for client in traffic_dict[app]:
        #print(client)
#for each client, loop through the servers it connects to/
        for traffic in traffic_dict[app][client]:
            userParams=initUserParams()
            #print(userParams)
            #print(traffic)
            print("\nApp:%s Client:%s Server:%s \n"%(app,client, traffic["target"]))
            #print(traffic_dict[app][client]["params"])
            traffic
            rule={}
            flowClass={}
            QoS={}
            server = traffic["target"]

            numFlows = 1


            try:
                params=traffic["params"]
                #print(params)
                #print("Using parameters from config file")
                cliParams=params["client"]
                print("client: %s" % cliParams)
                svrParams=params["server"]
                print("server: %s" % svrParams)
                cliCMDs,srvCMDs=getPnukeParameters(cliParams,svrParams)

                # sometimes we want to duplicate the number of flows
                if "numFlows" in params and int(params["numFlows"]) > 1:
                    numFlows = int(params["numFlows"])
                    print("numFlows: %d" % numFlows)

            except:

                print("No parameters found. Using default parameters of application type: ",app)
                cliCMDs,srvCMDs=getDefaultPnukeParameters(app)


            if "scp" in app:
                exec_scp_filename=writeSCP(server)
                userParams["Filename"]=exec_scp_filename
            if "ssh-text-edit" in app:
                userParams["bursty"]=cliParams["bursty"]
                userParams["speed"]=cliParams["speed"]
                #print("update: ",userParams)
                textEditFileName=runTextEdit(client,server,userParams)
            if "video-streaming"in app:
                userParams["port"]=cliParams["port"]
                userParams["resolution"]=cliParams["resolution"]
                userParams["time"]=cliParams["time"]
                userParams["protocol"]=cliParams["protocol"]
            #if "video-streaming"in app:
             #   videoParams=getVideoEditParams(cliParams,svrParams)

            #print("client cmd: ",cliCMDs)
            #print("server cmd: ",cliCMDs)
            entitiesList=[]
            ruleCount=0
            #print(server)

            # generate the commands for servers and append them to a
            # dictionary value.
            #
            # commands will be repeated if the server target appears
            # multiple times (e.g., { h0 -> h1, h2 -> h1, h3 -> h1 })
            # but duplicate commands are removed just before
            # cc_protonuke.mm is generated.

            #Genrate the cmd for miniccc to exec protonuke
            cmdProtoServer= appSwitchServer(app,srvCMDs,userParams)

            cmdFilterServer="cc filter name="+server
            if cmdFilterServer in commandDict.keys():
                print("key: ",cmdFilterServer)
            else:
                commandDict[cmdFilterServer]=[]

            if cmdProtoServer!="None":
                commandDict[cmdFilterServer].append(cmdProtoServer)

            # generate the commands for the client portion
            cmdFilterClient="cc filter name="+client
            cmdProtoClient=appSwitchClient(app,server,cliCMDs,userParams)

            if cmdFilterClient in commandDict.keys():
                print("key: ",cmdFilterClient)
            else:
                commandDict[cmdFilterClient]=[]


            if cmdProtoClient != "None":
                commandDict[cmdFilterClient].extend([cmdProtoClient] * numFlows)

            # add cmdFilterClient to a set. later when we're trimming
            # duplicates, we want to keep all the duplicate protonuke
            # commands.
            #
            # alternatively, we can add some jitter to some of the
            # client parameters such that they won't get filtered out.
            clients.add(cmdFilterClient)

#Write the  mm file
f = open("cc_protonuke.mm","w")
#for each vm filter write the attached cmd
f.write("cc exec bash -c \"rm -rf /tmp/miniccc/files/"+serveFilesDir+"/video-streaming/\"\n")
f.write("cc send " + serveFilesDir + "protonuke\n")
print(json.dumps(commandDict, sort_keys=True, indent=4))
for filt in commandDict:
    f.write("clear cc filter\n")
    f.write(filt+"\n")

    #for cmd in commandDict[filt]:
    #    if "proto-server.sh"  in cmd:
    #        f.write(cmd)
    #        commandDict[filt].remove(cmd)


    # we want to be able to have duplicate protonuke commands in our
    # clients. (although we won't be able to have duplicates for
    # video-streaming because of the way it was coded).
    if filt not in clients:
        commandDict[filt]=list(set(commandDict[filt]))

    #print(commandDict[filt])
    for cmd in commandDict[filt]:
        #print(cmd)
        f.write(cmd+"\n")
f.close()
