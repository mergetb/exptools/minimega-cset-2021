#! /bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit -1
fi

# needs at least 1 arg:
# 1. vm name that does vnc commands
if [ "$#" -ne 1 ]; then
    echo "Must provide 1 argument: <vm-name>"
    echo "Ex: ./stop_ssh_vim.sh h0"
    exit -1
fi

this_dir=$(dirname $(readlink -f "$0"))
root=$this_dir/../../..
source $root/minimega/minimega.sh

WDIR="$this_dir/vnc"
VM=$1

function WAIT_VNC {
    # wait for previous vnc to finish
    while [ $(MM_CMD vnc | grep $VM | wc -l) -ne 0 ]; do
        sleep 5
    done
}

rm "$BASE/$VM-vnc_lock"

MM_CMD vnc stop $VM
WAIT_VNC
sleep 5
MM_CMD clear cc filter
MM_CMD cc filter name=$VM
MM_CMD cc exec pkill -f "ssh "
MM_CMD cc exec pkill "vim"
MM_CMD cc exec pkill "bash"
# python3 generate_vnc.py --speed 2 --enter --vimcmd $WDIR/vim_quit_$VM ":q!"
# sleep 2
# MM_CMD vnc play $VM $WDIR/vim_quit_$VM
# WAIT_VNC
# python3 generate_vnc.py --speed 2 --enter $WDIR/ssh_quit_$VM "exit"
# sleep 3
# MM_CMD vnc play $VM $WDIR/ssh_quit_$VM
# WAIT_VNC
# if [ $CLIENT_WANT_GUI = 1 ]; then
#     # quit again to exit from terminal
#     MM_CMD vnc play $VM $WDIR/ssh_quit_$VM
#     WAIT_VNC
# fi
