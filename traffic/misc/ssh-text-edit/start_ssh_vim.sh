#! /bin/bash

# This script should be run backgrounded. The intention is to write forever
# and thus ends in an infinite loop. To stop this script,
# remove the file at $BASE/$VM-vnc_lock or kill the process

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit -1
fi

# needs at least 2 arg:
# 1. vm name that does vnc commands
# 2. ip/hostname to ssh connect to
if [ "$#" -lt 2 ]; then
    echo "Must provide 2 arguments: <vm-name> <target-hostname> "
    echo """
    Positional arguments:
        vm-name: name of VM client who will initiate the ssh-vim session
        target-hostname: name of VM server which will host the ssh-vim session 

    Optional arguments:
        bursty: include random delays between text (9s delay per 10s) 
            to enable: [true,bursty]
            to disable: not any of the above
            default: false
        speed: typing speeds 
            possible values: [slow,medium,fast]
            default: fast

    Ex: ./start_ssh_vim.sh h0 h1 true medium &
    """
    exit -1
fi


this_dir=$(dirname $(readlink -f "$0"))
root=$this_dir/../../..
source $root/minimega/minimega.sh

WDIR="$this_dir/vnc"
mkdir -p $WDIR
VM=$1
TARGET=$2
#DIR=$PWD

LOREM_IPSUM_1="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam accumsan volutpat velit vel commodo. Sed auctor fringilla massa, in commodo nisi hendrerit id. Proin sollicitudin sagittis ante sed rhoncus. Maecenas feugiat scelerisque tempor. Integer aliquet nisl vitae diam condimentum, ut pharetra urna tristique. Nam suscipit aliquet finibus. Sed tristique lobortis orci, hendrerit imperdiet elit dapibus in. Sed at vestibulum nunc, sit amet varius nisl. Sed ornare nulla augue, consectetur ultricies magna bibendum nec. Donec porta est quam, sit amet pellentesque turpis sodales ac. Vestibulum pellentesque ante sagittis venenatis pellentesque. "
LOREM_IPSUM_2="Quisque faucibus ornare lectus, eget tristique neque sollicitudin in. Maecenas fermentum nulla accumsan, commodo velit et, rhoncus ipsum. Cras nec lacinia libero. Pellentesque tincidunt leo eget accumsan dignissim. Phasellus malesuada dui mauris, vel fringilla metus semper vehicula. Praesent nec odio laoreet metus blandit faucibus sit amet quis ligula. Curabitur nibh libero, finibus sit amet metus et, egestas pretium lacus. Nunc et risus ac quam cursus ullamcorper eu ut ante. Cras erat lorem, vehicula in vehicula vitae, sodales a nibh. Nulla ac ante euismod, viverra ex sed, porttitor elit. Etiam sagittis ac ligula quis pretium. Aenean fermentum odio eu est accumsan, vitae maximus nisi viverra. Morbi tincidunt enim vel sem rhoncus, eget porttitor diam sollicitudin. Nunc rhoncus, leo eu vestibulum efficitur, ligula risus blandit tortor, vitae consequat nibh justo sit amet turpis. "
LOREM_IPSUM_3="Etiam lacus nisi, mollis sit amet lorem nec, vestibulum venenatis turpis. Nunc molestie pulvinar massa, eu iaculis eros lobortis eget. Nunc pretium, massa egestas condimentum euismod, lorem massa scelerisque eros, ut egestas odio tortor eget nisl. Nam sed accumsan nisi, ac ultrices massa. Praesent non ornare nisi. Proin porta, velit ut dapibus pretium, velit purus ultricies quam, in pretium dolor sapien eu quam. Quisque in purus eget nunc rutrum cursus in a ipsum. Vivamus aliquet fermentum est, in semper sapien volutpat non. Vestibulum pretium, arcu vestibulum tempor eleifend, tellus libero vehicula leo, non semper nibh metus id lectus. Integer semper maximus nunc, sit amet ultricies orci bibendum et. Ut blandit erat vel est bibendum luctus. "
LOREM_IPSUM_4="Mauris tincidunt viverra imperdiet. Nam tellus est, aliquet a venenatis nec, pellentesque vel arcu. Ut efficitur neque nulla, id dignissim turpis tristique a. Aenean convallis mauris consequat lorem pretium auctor. Fusce pellentesque lacinia purus, nec suscipit risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris hendrerit pharetra tincidunt. Mauris et tortor quis risus dictum congue. Nulla facilisi. Etiam vitae elit eu mauris convallis efficitur ut dignissim metus. Morbi id ultricies dui. Praesent at odio scelerisque urna lobortis dapibus et nec nibh. Fusce maximus lobortis felis, quis porta magna scelerisque eu. Ut rutrum purus non neque iaculis, vitae volutpat nisi finibus. Donec venenatis magna in venenatis pulvinar. Aliquam erat volutpat. "
LOREM_IPSUM_5="Sed at nibh lorem. Nam a libero vitae lacus tincidunt rutrum. Nam venenatis est lacinia, interdum dui eu, congue lectus. Duis consequat enim at ligula cursus tristique. Nulla porttitor ut lectus sed cursus. Ut laoreet eros ut dolor eleifend pellentesque. Praesent tristique leo pharetra ipsum sagittis, a varius erat faucibus. "

function CHECK_STOP {
    # check if we should quit this script
    if [ $(MM_CMD vm info | grep $VM | wc -l) -eq 0 ]; then
        rm "$BASE/$VM-vnc_lock"
        MM_CMD vnc stop $VM
        exit 0
    fi
    if [ ! -f "$BASE/$VM-vnc_lock" ]; then
        MM_CMD vnc stop $VM
        exit 0
    fi
}

function WAIT_VNC {
    # wait for previous vnc to finish
    while [ $(MM_CMD vnc | grep $VM | wc -l) -ne 0 ]; do
        CHECK_STOP
        sleep 5
    done
}

PARAMS="--vimtext"
if [ "$#" -ge 5 ] && ([ $5 == "true" ] || [ $5 == "bursty" ]); then
    PARAMS="$PARAMS --bursty"
fi
if [ "$#" -ge 6 ]; then
    if [ $6 == "fast" ]; then
        PARAMS="$PARAMS --speed 3"
    fi
    if [ $6 == "medium" ]; then
        PARAMS="$PARAMS --speed 2"
    fi
    if [ $6 == "slow" ]; then
        PARAMS="$PARAMS --speed 1"
    fi
else
    PARAMS="$PARAMS --speed 3"
fi

python3 generate_vnc.py --guicmd $WDIR/gui_open_terminal_$VM "t"
python3 generate_vnc.py --enter $WDIR/enter_$VM ""
python3 generate_vnc.py --enter $WDIR/ssh_start_$VM "ssh -oStrictHostKeyChecking=no searchlight@$TARGET"

touch "$BASE/$VM-vnc_lock"

# starts an ssh session, opens vim, and types lorem ipsum forever
# creates lock on file system. to stop this script from running, delete lock from FS
MM_CMD vnc play $VM $WDIR/gui_open_terminal_$VM
WAIT_VNC
sleep 3
MM_CMD vnc play $VM $WDIR/enter_$VM
WAIT_VNC
MM_CMD vnc play $VM $WDIR/ssh_start_$VM
WAIT_VNC
sleep 3
python3 generate_vnc.py --enter $WDIR/vim_start_$VM "vim -n /tmp/test"
MM_CMD vnc play $VM $WDIR/vim_start_$VM
WAIT_VNC
while true; do 
    CHECK_STOP
    for i in {1..5}; do 
        var=LOREM_IPSUM_$i
        python3 generate_vnc.py $PARAMS $WDIR/vim_lorem_ipsum_$i_$VM "${!var}"
        MM_CMD vnc play $VM $WDIR/vim_lorem_ipsum_$i_$VM
        WAIT_VNC
        python3 generate_vnc.py --enter --vimcmd $WDIR/vim_write_$VM ":w!"
        MM_CMD vnc play $VM $WDIR/vim_write_$VM
        WAIT_VNC
    done
done
