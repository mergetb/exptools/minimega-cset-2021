Ground truth identification for SSH
Add the following to all users' ~/.bashrc.

```
if [[ -v SSH_CONNECTION ]]; then
    date=$(date +%s%N)
    app="ssh"
    protocol="tcp"
    srcip=$(echo $SSH_CONNECTION | awk '{print $1}')
    srcport=$(echo $SSH_CONNECTION | awk '{print $2}')
    dstip=$(echo $SSH_CONNECTION | awk '{print $3}')
    dstport=$(echo $SSH_CONNECTION | awk '{print $4}')
    if [[ ! $srcip == 8* ]]; then
            #echo "[FlowInfo] Initial Connection StartTime:$date EndTime:0 Application:$app Protocol:$protocol SrcIP:$srcip SrcPort:$srcport DstIP:$dstip DstPort:$dstport" >> /home/searchlight/ssh.log
            sudo sh -c "echo \"[FlowInfo] Initial Connection StartTime:$date EndTime:0 Application:$app Protocol:$protocol SrcIP:$srcip SrcPort:$srcport DstIP:$dstip DstPort:$dstport\" >> /home/searchlight/ssh.log >> /root/logs/ssh.log"
        fi
fi
```
