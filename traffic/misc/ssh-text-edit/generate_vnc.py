#! /usr/bin/python3

import random
import argparse

is_shift = {
"colon" :1,
"at" :1,
"exclam" :1,
"numbersign" :1,
"dollar" :1,
"percent" :1,
"ampersand" :1,
"parenleft" :1,
"parenright" :1,
"asterisk" :1,
"plus" :1,
"greater" :1,
"question" :1,
"braceleft" :1,
"braceright" :1,
"underscore" :1,
"quotedbl" :1
}

ascii_dictionary = {
"0xa":"Linefeed",
"0xd":"Return",
"0x20":"space",
"0x21":"exclam",
"0x22":"quotedbl",
"0x23":"numbersign",
"0x24":"dollar",
"0x25":"percent",
"0x26":"ampersand",
"0x27":"apostrophe",
"0x27":"quoteright",
"0x28":"parenleft",
"0x29":"parenright",
"0x2a":"asterisk",
"0x2b":"plus",
"0x2c":"comma",
"0x2d":"minus",
"0x2e":"period",
"0x2f":"slash",
"0x30":"0",
"0x31":"1",
"0x32":"2",
"0x33":"3",
"0x34":"4",
"0x35":"5",
"0x36":"6",
"0x37":"7",
"0x38":"8",
"0x39":"9",
"0x3a":"colon",
"0x3b":"semicolon",
"0x3c":"less",
"0x3d":"equal",
"0x3e":"greater",
"0x3f":"question",
"0x40":"at",
"0x41":"A",
"0x42":"B",
"0x43":"C",
"0x44":"D",
"0x45":"E",
"0x46":"F",
"0x47":"G",
"0x48":"H",
"0x49":"I",
"0x4a":"J",
"0x4b":"K",
"0x4c":"L",
"0x4d":"M",
"0x4e":"N",
"0x4f":"O",
"0x50":"P",
"0x51":"Q",
"0x52":"R",
"0x53":"S",
"0x54":"T",
"0x55":"U",
"0x56":"V",
"0x57":"W",
"0x58":"X",
"0x59":"Y",
"0x5a":"Z",
"0x5b":"bracketleft",
"0x5c":"backslash",
"0x5d":"bracketright",
"0x5e":"asciicircum",
"0x5f":"underscore",
"0x60":"grave",
"0x60":"quoteleft",
"0x61":"a",
"0x62":"b",
"0x63":"c",
"0x64":"d",
"0x65":"e",
"0x66":"f",
"0x67":"g",
"0x68":"h",
"0x69":"i",
"0x6a":"j",
"0x6b":"k",
"0x6c":"l",
"0x6d":"m",
"0x6e":"n",
"0x6f":"o",
"0x70":"p",
"0x71":"q",
"0x72":"r",
"0x73":"s",
"0x74":"t",
"0x75":"u",
"0x76":"v",
"0x77":"w",
"0x78":"x",
"0x79":"y",
"0x7a":"z",
"0x7b":"braceleft",
"0x7c":"bar",
"0x7d":"braceright",
"0x7e":"asciitilde",}

# speed arg: 3 - fast; 2 - medium; 1 - slow; 0 - pause
# fast: 400 char/min [ 1/(400/60) = 0.15 avg seconds between char ]
# medium: 200 char/min
# slow: 60 char/min
# pause: 8-10 second pause
def get_speed(speed):
    if speed == 3:
        # distribution between 0.0 - 0.30s
        return random.randint(0, 300000000)
    elif speed == 2:
        # distribution between 0.15 - 0.45s
        return random.randint(150000000, 450000000)
    elif speed == 1:
        # distribution between 0.85 - 1.15s
        return random.randint(850000000,1150000000)
    else:
        # distribution between 8 - 10s
        return random.randint(8000000000,10000000000)

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a VNC playback session for keyboard movements only.")
    parser.add_argument('filename', type=str, help="Filename to write vnc playback session.")
    parser.add_argument('text', type=str, help="Text to write.")
    parser.add_argument('--speed', dest="speed", type=int, nargs="?", default=3, required=False, help="Speed at which to type at. 3=fast, 2=medium, 1=slow.")
    parser.add_argument('--vimtext', dest="vimtext", action="store_true", help="Include VIM behavior in text writing, such as start with ESC and new line, end with ESC, page ups/downs every once in a while.")
    parser.add_argument('--vimcmd', dest="vimcmd", action="store_true", help="Wrap VIM command with ESC.")
    parser.add_argument('--guicmd', dest="guicmd", action="store_true", help="Wrap command with CTRL+META. Helpful for gui quick commands.")
    parser.add_argument('--bursty', dest="bursty", action="store_true", help="Include random delays between text")
    parser.add_argument('--enter', dest="enter", action="store_true", help="Add carraige return character to the end of the vnc playback")
    args = parser.parse_args()
    filename = args.filename
    text = args.text
    vimtext = args.vimtext
    vimcmd = args.vimcmd
    bursty = args.bursty
    speed = args.speed
    enter = args.enter
    guicmd = args.guicmd

    ns_counter=0
    bursty_counter=0
    bursty_threshold=10*(10**9) # 10 seconds
    vim_counter=0
    vim_threshold=100 # 100 characters

    with open(filename, "w+") as f:
        if guicmd:
            f.write("100:KeyEvent,true,Control_L\n")
            f.write("100:KeyEvent,true,Meta_L\n")
        if vimcmd:
            f.write("100:KeyEvent,true,Escape\n")
            f.write("100:KeyEvent,false,Escape\n")
        if vimtext:
            f.write("100:KeyEvent,true,Escape\n")
            f.write("100:KeyEvent,false,Escape\n")
            f.write("100:KeyEvent,true,o\n")
            f.write("100:KeyEvent,false,o\n")
        for i in text:
            delay=get_speed(speed)
            if bursty:
                bursty_counter += delay
                if bursty_counter > bursty_threshold:
                    bursty_counter = 0
                    delay = get_speed(0) # pause
            if vimtext:
                vim_counter += 1
                if vim_counter > vim_threshold:
                    vim_counter = 0
                    # page up / page down
                    f.write("100:KeyEvent,true,Escape\n")
                    f.write("100:KeyEvent,false,Escape\n")
                    f.write("100:KeyEvent,true,Control_L\n")
                    # go page up down slower
                    f.write(str(get_speed(1)) + ":KeyEvent,true,b\n")
                    f.write("100:KeyEvent,false,b\n")
                    f.write(str(get_speed(1)) + ":KeyEvent,true,f\n")
                    f.write("100:KeyEvent,false,f\n")
                    f.write("100:KeyEvent,false,Control_L\n")
                    f.write("100:KeyEvent,true,Escape\n")
                    f.write("100:KeyEvent,false,Escape\n")
                    f.write("100:KeyEvent,true,o\n")
                    f.write("100:KeyEvent,false,o\n")
            i = ascii_dictionary[hex(ord(i))]
            if i in is_shift and is_shift[i]:
                f.write("100:KeyEvent,true,Shift_L\n")
            f.write(str(delay) + ":KeyEvent,true," + i + "\n")
            f.write("100:KeyEvent,false," + i + "\n")
            if i in is_shift and is_shift[i]:
                f.write("100:KeyEvent,false,Shift_L\n")
        if guicmd:
            f.write("100:KeyEvent,false,Control_L\n")
            f.write("100:KeyEvent,false,Meta_L\n")
        if vimtext:
            f.write("100:KeyEvent,true,Escape\n")
            f.write("100:KeyEvent,false,Escape\n")
        if enter:
            f.write("100:KeyEvent,true,Return\n")
            f.write("100:KeyEvent,false,Return")
