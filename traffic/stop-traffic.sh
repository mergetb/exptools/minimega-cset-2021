#!/bin/bash

this_dir=$(dirname $(readlink -f "$0"))
root=$this_dir/..
source $root/minimega/minimega.sh

###
# Check if we're running as root
if [ $EUID -ne 0 ]; then
    echo "Must be run as root"
    exit 1
fi

###
# Stop protonuke
MM_CMD clear cc filter
MM_CMD cc exec killall protonuke

###
# Stop Caddy webserver
MM_CMD cc exec pkill caddy

###
# Stop scp
MM_CMD cc exec pkill scp
MM_CMD cc exec pkill -9 -f exec_scp

###
# Stop web browsers and automation tools
MM_CMD cc exec pkill chrome
MM_CMD cc exec pkill node
MM_CMD cc exec pkill run.sh

###
# Stop flow ingester
MM_CMD cc exec killall NtupleIngester

###
# Stop SSH text editing 
if [ $(ps -eo args | grep "start_ssh_vim" | grep -v "grep" | wc -l) -ne 0 ]; then
    ps -eo args | grep "start_ssh_vim" | grep -v "grep" | while read -r line; do
        client_vm=$(echo "$line" | awk -F" " '{print $3}')
        pushd misc/ssh-text-edit
        ./stop_ssh_vim.sh $client_vm
        popd
    done
fi

rm -f $BASE/files/miniccc_files/exec_scp*
