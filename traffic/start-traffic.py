#!/usr/bin/env python3

import sys
import os
import subprocess
import contextlib
import json
import time
import getopt

@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)

def run_miniccc_command(cmd):
    print("cmd=%s" % cmd)
    try:
        result = subprocess.run([
            "../bin/minimega",
            "-base", "/tmp/minimega",
            "-e", cmd
        ], check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        print("Failed to run miniccc command: %s" % e.output)
        return None, False

    return result, True

def generate_traffic_config(traffic_f):
    try:
        subprocess.run([
            "python3",
            "misc/Auto-Traffic-Generator.py",
            traffic_f,
            "misc/Supported-Applications.json",
        ], check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        print("Failed to run Auto-Traffic-Generator: %s" % e.output)
        return False

    os.rename("cc_protonuke.mm", "misc/cc_protonuke.mm")
    return True

def start_ssh_text_edit_traffic(traffic_file):
    params = []
    clients = []

    with open(traffic_file) as f:
        data = json.load(f)

    # Return an empty list if we don't have any ssh-text-edit blocks.
    if "ssh-text-edit" not in data.keys():
        return clients

    try:
        for client in data["ssh-text-edit"]:
            for conn in data["ssh-text-edit"][client]:
                server = conn["target"]
                bursty = conn["params"]["client"]["bursty"]
                speed  = conn["params"]["client"]["speed"]

                params.append([client, server, bursty, speed])

                # keep track of clients in a list so we can eventually
                # pass the list on to stop_ssh_text_edit_traffic.
                clients.append("%s" % client)
    except KeyError:
        print("Invalid traffic.json ssh-text-edit format")
        return None

    with pushd("misc/ssh-text-edit"):
        for param in params:
            try:
                # use subprocess.Popen instead of subprocess.run because
                # we need this to run continuously in the background
                subprocess.Popen([
                    "bash",
                    "start_ssh_vim.sh"
                    ] + param,
                    stdout=subprocess.PIPE
                )
            except subprocess.CalledProcessError as e:
                print("Failed to run Auto-Traffic-Generator: %s" % e.output)
                return False

    return clients

def stop_ssh_text_edit_traffic(clients):
    if len(clients) == 0:
        return True

    print("Stopping ssh-text-edit on clients %s, ignore any error messages." % clients)

    with pushd("misc/ssh-text-edit"):
        for client in clients:
            try:
                subprocess.run([
                    "bash",
                    "stop_ssh_vim.sh",
                    "%s" % client
                    ], check=True, stdout=subprocess.PIPE)
            except subprocess.CalledProcessError as e:
                print("Failed to stop ssh-text-edit: %s" % e.output)
                return False

    return True

def start_traffic(traffic_fname):
    result, status = run_miniccc_command("read %s" % traffic_fname)
    if not status:
        print("Failed to read protonuke .mm into minimega")
        return False

    return True

def stop_traffic():
    try:
        subprocess.run([
            "./stop-traffic.sh"
        ], check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        print("Failed to run ./stop-traffic.sh: %s" % e.output)
        return False

    return True

def usage(argv, out_f=sys.stderr, exit_code=1):
    msg  = "Usage: %s [OPTIONS] <traffis JSON>\n" % argv[0]
    msg += "  (-h, --help)  : print help and exit\n"
    msg += "  (-t, --time=) : time (in s) after traffic starts to end of experiment (default: 120)"

    print(msg, file=out_f)
    sys.exit(exit_code)

def parse_cmd_line(argv):
    opts = []
    args = []

    options = {
        "time" : 120,
    }

    try:
        opts, args = getopt.getopt(
            argv[1:],
            "ht:",
            ["help", "time="]
        )
    except getopt.GetoptError as err:
        usage(argv)

    for o, a in opts:
        if o in ("-h", "--help"):
            usage(argv, out_f=sys.stdout, exit_code=0)

        elif o in ("-t", "--time"):
            options["time"] = int(a)

        else:
            usage(argv)

    if len(args) != 1:
        usage(argv)

    return args[0], options

def main(argv):
    # Check for root user first
    if os.geteuid() != 0:
        print("Run as root user")
        return 1

    traffic_f, options = parse_cmd_line(argv)
    traffic_time = options["time"]

    print("Generating traffic config")
    if not generate_traffic_config(traffic_f):
        print("Failed to generate traffic config")
        return 1

    ssh_text_edit_clients = start_ssh_text_edit_traffic(os.path.abspath(traffic_f))
    if ssh_text_edit_clients is None:
        print("Failed to start ssh-text-edit traffic")
        return 1

    print("Starting traffic")
    if not start_traffic(os.path.abspath("misc/cc_protonuke.mm")):
        print("Failed to start traffic")
        return 1

    print("Letting traffic run for %d sec before ending experiment" % traffic_time)
    time.sleep(traffic_time)

    print("Stopping traffic ...")

    if not stop_traffic():
        print("Failed to stop traffic")
        return 1
    
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
