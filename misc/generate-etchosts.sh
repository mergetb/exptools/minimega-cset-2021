#!/bin/bash

###
# Generate a hosts file from the running topology

if [ $EUID -ne 0 ]; then
    echo "Must be run as root"
    exit 1
fi

mmod_dir=$(readlink -fm $(dirname $0))/../minimega
source $mmod_dir/minimega.sh

hostsf="hosts.txt"

ips=($(MM_CMD .columns name,ip vm info | awk -F "|" '{print $3}' | tail -n +2 | tr -d "^ " | tr -d "[" | tr -d "]"))
hosts=($(MM_CMD .columns name,ip vm info | awk -F "|" '{print $2}' | tail -n +2 | tr -d "^ "))

num_hosts=${#hosts[@]}

cat > $hostsf << EOF
#
# Auto-generated hosts file
# Append this to /etc/hosts
#
EOF

for i in $(seq 0 $((num_hosts-1))); do
    host=${hosts[$i]}
    host_ips=${ips[$i]}
    ctrl_net=$(echo $host_ips | awk -F "," '{print $1}')

    echo "$ctrl_net $host-ctrl" >> $hostsf
done
