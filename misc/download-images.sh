#!/bin/bash

mkdir -p img
pushd img
curl -OL https://storage.googleapis.com/searchlightpharos/ubuntu18.04-searchlight-client-gui_2021-07-08.qcow2
curl -OL https://storage.googleapis.com/searchlightpharos/ubuntu18.04-searchlight-router.qcow2
popd
