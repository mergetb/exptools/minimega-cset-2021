# Case Studies in Experiment Design on a minimega Based Network Emulation Testbed

This page provides instructions to define the experiment topologies and application traffic
described in the 2021 CSET paper "Case Studies in Experiment Design on a minimega Based Network
Emulation Testbed"


## Paper

https://doi.org/10.1145/3474718.3474730

## Prerequisites

There are a few prerequisites to run the examples discussed in this webpage:
- [Software](#software)
- [Images](#images)
- [Settings](#settings)
- [Resources](#resources)


### Software

- You need to first install minimega. See the minimega documentation here:
  https://tip.minimega.org/articles/installing.article. The minimega binary must be available at
  `<this-directory>/bin`, next to pre-existing binaries `protonuke` and `miniweb`

- Other software dependencies include:
    - jq
    - ansible-playbook
### Images

To run the experiments described on this webpage, you need to download 2 qcow2 images: an image for
end-hosts, which generate and/or serve traffic, and an image for routers that make up the
core network. Both images are based on generic Ubuntu 18.04 LTS images but include a small set of
modifications to operate in our environment.

The end-host image includes:
- [miniccc](https://tip.minimega.org/articles/tutorials/cc.article) systemd service
- [Big Buck Bunny](https://en.wikipedia.org/wiki/Big_Buck_Bunny) video files
- Installation of the Gnome desktop environment and Google Chrome web browser
- `searchlight` user account and SSH pub/priv keypair

The router image includes:
- [miniccc](https://tip.minimega.org/articles/tutorials/cc.article) systemd service
- [minirouter](https://tip.minimega.org/articles/router.article) systemd service
- `searchlight` user account and SSH pub/priv keypair

You must download these images by running the following script:
```shell
./misc/download-images.sh
```

### Settings

Your host machine's firewall needs to either be disabled or configured to:
- allow DHCP traffic
- allow DNS traffic
- allow IP masquerading to provide Internet access in your VMs

On Debian/Ubuntu:
```
sudo ufw allow dns
sudo ufw allow dhcp
```

On Fedora/RH distributions:
```
sudo firewall-cmd --add-service=dns --permanent
sudo firewall-cmd --add-service=dhcp --permanent
sudo firewall-cmd --add-masquerade --permanent
sudo firewall-cmd --reload
```

Our minimega startup scripts will take care of configuring any other network device specific
settings needed to support these services in your experiments

### Resources

The examples in this repository are designed to be run on a single relatively powerful machine. The
minimum requirement is
- `>=` 12 cores
- `>=` 24 GB RAM

However, we recommend a machine with
- `>=` 24 cores
- `>=` 32 GB RAM

## Overview

The two main contributions of this work are a set of models that allow you to define network
topologies and traffic configurations.

- [networks](./networks) describes the network definition files for the topologies described in the paper.
- [traffic](./traffic) describes the traffic definition files for the topologies described in the
paper.

See the networks link for examples on setting up a network topology. Then, see the traffic link for
examples on how to run traffic applications on the network you created.
