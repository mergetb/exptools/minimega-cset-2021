#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <path to topology>"
    exit 1
fi

root=$(dirname "$(readlink -f "$0")")/../
topo=$(readlink -fm $1)
if [ ! -f $topo ]; then
    echo "No such file '$topo'"
    exit 1
fi

source $root/minimega/minimega.sh

echo "-- setting permissions on $BASE"
mkdir -p $BASE
chmod 755 $BASE

echo "-- initializing mega_bridge"
ovs-vsctl add-br mega_bridge
ovs-vsctl set-fail-mode mega_bridge secure
ovs-ofctl del-flows mega_bridge
ovs-ofctl add-flow mega_bridge "table=0, priority=0, actions=NORMAL"

iface=$(ip -j route show default | jq -r '.[0].dev') 
echo "-- enabling NAT through device $iface"
sysctl -w net.ipv4.ip_forward=1 &> /dev/null
iptables -t nat -A POSTROUTING -o $iface -j MASQUERADE
iptables -A INPUT -i $iface -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -j ACCEPT

echo "-- disabling ipv6"
sysctl net.ipv6.conf.all.disable_ipv6=1

echo "-- starting minimega"
$root/bin/minimega -nostdin -base $BASE &> /dev/null &

sleep 2

echo "-- starting miniweb"
$root/bin/miniweb -base $BASE -root $root/misc/web &

echo "-- creating control network"
MM_CMD tap create 100 bridge mega_bridge  ip 8.0.0.1/16 ctrl0 
iptables -A INPUT -i ctrl0 -j ACCEPT

MM_CMD dnsmasq start 8.0.0.1 8.0.0.2 8.0.254.254
MM_CMD dnsmasq configure 0 options option:router

echo "-- setting up traffic support files"
ln -s $root/traffic/miniccc_files $BASE/files/miniccc_files
cp $root/bin/protonuke $BASE/files/miniccc_files

echo "-- initializing VM configuration"
MM_CMD clear vm config
MM_CMD vm config memory 1024
MM_CMD vm config vcpus 1
MM_CMD vm config qemu-override "-netdev tap" "-netdev tap,vhost=on"
MM_CMD vm config snapshot true

IMG=$root/img
vm_idx=0
vlan_idx=0
source $topo

echo "-- restarting DNS server"
kill -s HUP $(pidof dnsmasq)

echo "-- starting VMs"
MM_CMD vm start all
