#!/bin/bash

BASE=/tmp/minimega

### Helper functions
# Check if user is root
function ROOT_CHECK {
    if [ ! -z "$MM_NOROOT" ]; then
        if [ "$MM_NOROOT" -ne 1 ]; then
            if [ $EUID -ne 0 ]; then
                echo "Minimega must be run as root"
                exit 1
            fi
        fi
    fi
}

# Run command on specific host
function HOST_MM_CMD {
    ROOT_CHECK

    host="$1"

    if [ "$host" = "$HOSTNAME" ]; then
        minimega -e "${@:2}"
    else
        minimega -e mesh send $host "${@:2}"
    fi
}

# Run namespace-aware command on local node
function MM_CMD {
    ROOT_CHECK
    minimega -base $BASE -e "$@"
}

# Run command across whole namespace
function NS_CMD {
    ROOT_CHECK
    MM_CMD ns run "$@"
}

# Restart dnsmasq
function restart_dnsmasq() {
    mask=($(pidof dnsmasq))
    for pid in ${mask[@]}; do
        kill -s HUP $pid
    done
}

###
# Utility to generate random MAC address
function get_random_macaddr()
{
    echo -n 02; dd bs=1 count=5 if=/dev/urandom 2>/dev/null |hexdump -v -e '/1 ":%02X"'
}

###
# Utility to setup a VMs nics:
#  1) disable offload
#  2) set VM MTUs
#  3) set host MTUs
#  4) disable RP filtering
function initialize_vm_nics()
{
    local vm="$1"

    MM_CMD cc filter name=$vm

    # Get list of VM nics from MM
    local taps=$(MM_CMD .filter name=$vm .columns name,tap vm info | tail -n +2 | awk -F "|" '{print $3}')
    local taps_ar=($(echo $taps | tr -d "[],"))

    if [ ! -z $MMOD_WANT_JUMBO_FRAMES ] && [ $MMOD_WANT_JUMBO_FRAMES -eq 1 ]; then
        local mtu=9166
    else
        local mtu=1450
    fi

    local nr=0
    local cmd=""
    for tap in ${taps_ar[@]}; do
        local nic="eth$nr"

        # VM nic offload + MTU
        MM_CMD cc exec ethtool -K $nic tx off rx off tso off gso off gro off sg off
        MM_CMD cc exec ip link set mtu $mtu dev $nic

        local cmd="$cmd ip link set mtu $mtu dev $tap; "
        local nr=$(($nr+1))

        # Disable RP filtering
        MM_CMD cc exec sysctl net.ipv4.conf.$nic.rp_filter=0
    done
    MM_CMD cc exec sysctl net.ipv4.conf.all.rp_filter=0
    MM_CMD cc exec sysctl net.ipv4.conf.default.rp_filter=0
    MM_CMD clear cc filter

    # Host MTU
    local host=$(MM_CMD .filter name=$vm .columns name vm info | tail -n +2 | awk -F "|" '{print $1}')
    ssh $host "$cmd"
}

function save_vm()
{
    local vm="$1"
    MM_CMD vm config save $vm
}

function restore_vm()
{
    local vm="$1"
    MM_CMD vm config restore $vm
}

function set_hostname()
{
    local vm="$1"

    MM_CMD cc filter name=$vm
    MM_CMD cc exec bash -c "echo $vm > /etc/hostname"
    MM_CMD cc exec hostname $vm
    MM_CMD clear cc filter
}

function colocate_vm_with()
{
    local vm="$1"
    local tgt="$2"

    restore_vm $vm
    MM_CMD vm config colocate $tgt
    save_vm $vm
}

function launch_vm()
{
    local vm="$1"

    restore_vm $vm
    MM_CMD vm launch kvm $vm
    initialize_vm_nics $vm
    set_hostname $vm
}

function launch_vm_with()
{
    local vm="$1"
    local tgt="$2"

    colocate_vm_with $vm $tgt
    launch_vm $vm
}

function get_net_str()
{
    local vm="$1"

    restore_vm $vm
    local net_str=$(MM_CMD vm config net)
    local nets=$(echo ${net_str#*:} | tr -d "[" | tr -d "]")

    echo "$nets"
}

function get_num_nics()
{
    local vm="$1"
    local nets=($(get_net_str $vm))

    echo "${#nets[@]}"
}

function get_mac_addr()
{
    local vm="$1"
    local nic="$2"

    local nets=($(get_net_str $vm))
    local net=${nets[$nic]}
    echo "$(echo $net | cut -d "," -f 2)"
}

function get_taps_str()
{
    local vm="$1"
    
    local tap_str=$(MM_CMD ".json true .filter name=$vm .columns name,tap vm info" | jq -r .[0].Tabular[0][1])
    local taps=$(echo $tap_str | tr -d "[" | tr -d "]" | tr -d ",")

    echo "$taps"
}

function get_tap()
{
    local vm="$1"
    local nic="$2"

    local taps=($(get_taps_str $vm))
    local tap=${taps[$nic]}
    echo $tap
}

function connect_vms()
{
    ###
    # add a new NIC to each VM's config, tagged with the same vlan id
    local vlan=$((101 + $vlan_idx)); let vlan_idx++

    for vm in $@; do
        restore_vm $vm
        local n=($(get_net_str $vm))
        MM_CMD vm config net ${n[@]} \
            $MEGA_BRIDGE,$vlan,$(get_random_macaddr),virtio-net-pci
        save_vm $vm
    done
}

function mark_v2v()
{
    local vm1="$1"
    local nic1="$2"
    local vm2="$3"
    local nic2="$4"

    local tap1=$(get_tap $vm1 $nic1)
    local tap2=$(get_tap $vm2 $nic2)
    local port1=$(ovs-ofctl show mega_bridge | grep "$tap1" | awk -F '(' '{print $1}' | tr -d ' ')
    local port2=$(ovs-ofctl show mega_bridge | grep "$tap2" | awk -F '(' '{print $1}' | tr -d ' ')

    ovs-ofctl add-flow mega_bridge "table=0, priority=1, in_port=$port1, actions=output:$port2"
    ovs-ofctl add-flow mega_bridge "table=0, priority=1, in_port=$port2, actions=output:$port1"
}

function new_vm_ctrlip()
{
    local vm="$1"
    local ip="$2"
    local mac=$(get_random_macaddr)

    # put it on control net
    MM_CMD vm config net mega_bridge,100,$mac,virtio-net-pci
    save_vm $vm

    # give it a known IP
    MM_CMD dnsmasq configure 0 ip $mac $ip 
}

function new_vm()
{
    local vm="$1"
    local idx=$((2 + vm_idx)) ; let vm_idx++
    new_vm_ctrlip $vm "8.0.0.$idx"
}

function new_vm_ctrl_dns()
{
    local vm="$1"
    local idx=$((2 + vm_idx)) ; let vm_idx++
    local ip="8.0.0.$idx"
    new_vm_ctrlip $vm $ip
    MM_CMD dnsmasq configure 0 dns $ip $vm
}


function set_minirouter_info_cost()
{
    local vm="$1"
    local nic="$2"
    local ip="$3"
    local mask="$4"
    local cost="$5"

    MM_CMD router $vm interface $nic "$ip/$mask"
    MM_CMD router $vm route ospf 0 $nic cost $cost

    #if [ $nic -eq 1 ]; then
    MM_CMD dnsmasq configure 0 dns $ip $vm
    #fi
}

function set_minirouter_info()
{
    local vm="$1"
    local nic="$2"
    local ip="$3"
    local mask="$4"

    set_minirouter_info_cost $vm $nic $ip $mask 10
}

function set_dnsmasq_client()
{
    local vm="$1"
    local ip="$2"
    local cli="$3"
    local cli_mac="$4"
    local cli_ip="$5"

    MM_CMD dnsmasq configure 0 dns $cli_ip $cli
    MM_CMD router $vm dhcp $ip static $cli_mac $cli_ip
}

function enable_nat()
{
    local vm="$1"

    MM_CMD cc filter name=$vm
    MM_CMD cc exec iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    MM_CMD cc exec iptables -A INPUT -i eth0 -m state --state ESTABLISHED,RELATED -j ACCEPT
    MM_CMD cc exec iptables -A OUTPUT -j ACCEPT
    MM_CMD clear cc filter
}
