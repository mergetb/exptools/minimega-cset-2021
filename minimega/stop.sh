#!/bin/bash

this_dir=$(dirname "$(readlink -f "$0")")
source $this_dir/minimega.sh

pid=$(pidof minimega)
if [ $? -eq 0 ]; then
    echo "-- stopping minimega"
    MM_CMD quit
fi

pid=$(pidof miniweb)
if [ $? -eq 0 ]; then
    echo "-- stopping miniweb"
    kill -s TERM $pid
fi

echo "-- enabling ipv6"
sysctl net.ipv6.conf.all.disable_ipv6=0

iface=$(ip -j route show default | jq -r '.[0].dev') 
echo "-- disabling NAT through device $iface"
sysctl -w net.ipv4.ip_forward=0 &> /dev/null
iptables -t nat -D POSTROUTING -o $iface -j MASQUERADE
iptables -D INPUT -i ctrl0 -j ACCEPT
iptables -D INPUT -i $iface -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -D OUTPUT -j ACCEPT

echo "-- removing mega_bridge"
ovs-vsctl del-br mega_bridge 2> /dev/null

rm -rf $BASE
